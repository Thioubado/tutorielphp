<?php
class Message {

    const LIMIT_USERNAME = 3;
    const LIMIT_MESSAGE = 10;

    private $username;
    private $message;


    // le point d'interroggation devant DateTime veut dire que cela peut être null
    public function __construct(string $username, string $message, ?DateTime $date = null)
    {
        $this->username = $username;
        $this->message = $message;
    }

    public function isValid(): bool
    {
        return strlen($this->username) > self::LIMIT_USERNAME && strlen($this->message) >= self::LIMIT_MESSAGE;
    }

    public function getErrors(): array
    {
        $errors = [];

        if (strlen($this->username) < self::LIMIT_USERNAME) {
            $errors['username'] = "Votre pseudo est trop court";
        }

        if (strlen($this->message) < self::LIMIT_MESSAGE) {
            $errors['message'] = "Votre messsage est trop court";
        }

        return $errors;

    }
}