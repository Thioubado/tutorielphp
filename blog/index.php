<?php 
$pdo = new PDO('mysql:host=127.0.0.1;dbname=data.db', 'root', '', [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
]);

$error = null;
try {

    if(isset($_POST['name'], $_POST['content']))
    {
        $query = $pdo->prepare("INSERT INTO posts (name, content, created_at) VALUES (:name, :content, :created_at)");
        $query->execute([
            "name" => $_POST['name'],
            "content" => $_POST['content'],
            "created_at" => time()
        ]);

        header("Location:./edit.php?id=" . $pdo->lastInsertId());
        exit();
    }

    //code...
    $query = $pdo->query("SELECT * FROM posts");
    $posts = $query->fetchAll();
} catch (PDOException $e) {
    $error = $e->getMessage();
}




require '../elements/header.php'; 

?>
<div class="container">
<?php if($error): ?>
    <div class="alert alert-danger">
        <?= $error ; ?>
    </div>
<?php else :?>
    <ul>
        <?php foreach ($posts as $post) : // j'ai enlevé le /blog/edit.php:?>
            <li><a href="./edit.php?id=<?= $post->id ?>"><?= htmlentities($post->name) ?></a></li>
        <?php endforeach ?>
    </ul>

    <form action="" method = "post">
            <div class="form-groupe">
            <input type="text" class="form-control" name='name' value="">
            </div>
            <div class="form-groupe">
            <textarea type="text" class="form-control" name='content' value=""></textarea>
            </div>
            <button class="btn btn-primary">Sauvegarder</button>
        </form>


<?php endif ?>
</div>

<?php require '../elements/footer.php' ;?>